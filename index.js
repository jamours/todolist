// ---------- Tous les Initialisations de variables ----------

// Initialisation de variables qui contiendra la valeur d'une variable HTML , "document.getElementById" recupere le contenue d'une variable precise
const taskList = document.getElementById("task-list");
const newTaskInput = document.getElementById("new-task");
const addTaskButton = document.getElementById("add-task");
const allTasksButton = document.getElementById("all-tasks");
const completedTasksButton = document.getElementById("completed-tasks");
const uncompletedTasksButton = document.getElementById("uncompleted-tasks");
const searchTaskInput = document.getElementById("search-task");
const searchButton = document.getElementById("search-button");
const filters = [allTasksButton, completedTasksButton, uncompletedTasksButton]; // initialisation d'un tableau (car [] ) qui contiendra la valeur de chaque varaible
let tasks = []; // initialisation d'un tableau vide

// ---------- Toutes les fonctions ----------

// Fonction pour ajouter une tâche à la liste
function addTask() {
  const taskName = newTaskInput.value.trim(); // on créé une variable taskName qui contient ce qui a été entré dans le html "new-task", il check si c'est vide avec le trim()
  if (taskName) {
    // si taskName contient quelque chose...
    const task = {
      // creation d'une variable task
      name: taskName, // qui contient en nom le nom de la taskName donc ce qu'on a entré dans le html "new-task"
      completed: false, // qui n'est pas " complété "
    };
    tasks.push(task);
    //Si la valeur est valide, une tache est créé avec son nom, la catégorie "" par défaut et un état de complétion initial à "false"
    // Cette tâche est ensuite ajouté à la fin du tableau "tasks" à l'aide de la méthode "push()"
    displayTasks();
    saveTasks();
    newTaskInput.value = "";
    // la fonction displayTasks() est appelée pour mettre à jour l'affichage de la liste des tâches car a chaque création de tache, il doit l'actualiser
    // La fonction saveTasks() est également appelée pour sauvegarder la nouvelle tâche dans le stockage local du navigateur
    // Le contenu de l'élément HTML "input" est réinitialisé à une chaîne de caractcere vide pour préparer l'ajout d'une nouvelle tâche
  }
}

// Cette fonction  displayAllTasks() est appelée lorsque l'utilisateur clique sur le bouton "Toutes" qui affiche toutes les taches
function displayAllTasks() {
  displayTasks(tasks);
}

// Cette fonction  displayCompletedTasks() est appelée lorsque l'utilisateur clique sur le bouton "Terminées" qui affiche les taches terminées
function displayCompletedTasks() {
  const completedTasks = tasks.filter((task) => task.completed);
  displayTasks(completedTasks);
  // La ligne 38 crée une nouvelle variable "completedTask"s" qui contient toutes les tâches de la liste "tasks" qui sont marquées comme terminées ( lorsqu'on clique sur terminé l'état de la ligne 17 completed : false devient completed : true)
  // On utilise la méthode filter() pour créer un nouveau tableau ( car on a initialisé "tasks" en tant que tableau) contenant seulement les tâches filtrés, qui est ici que la tâche doit être marquée comme terminée.
  // La ligne 40 appelle la fonction displayTasks() en lui passant le tableau "completedTasks" pour afficher les tâches dans le HTML
}

// Cette fonction  displayUncompletedTasks() est appelée lorsque l'utilisateur clique sur le bouton "En cours" qui affiche les taches
function displayUncompletedTasks() {
  const uncompletedTasks = tasks.filter((task) => !task.completed);
  displayTasks(uncompletedTasks);
  // Cette fonction affiche les tâches non terminées en filtrant les tâches stockées dans le tableau "tasks" en utilisant la méthode "filter"
  // Les tâches non terminées sont stockées dans un nouveau tableau appelé "uncompletedTasks"
  // Enfin, la fonction "displayTasks" est appelée pour afficher le contenu du tableau "uncompletedTasks"
  // En gros, cette fonction fait juste le contraire de celle juste au dessus car il y a le "!" qui fait la négation
}

// Fonction pour afficher les tâches dans la liste
function displayTasks(tasksToShow = tasks) {
  // creation d'une fonction qui montre les taches et qui cree une variable pour la fonction qui contient les valeurs de "tasks" qui est un tableau
  taskList.innerHTML = ""; // On vide la taskList
  tasksToShow.forEach((task, index) => {
    // Pour chaque taches du tableau tasks
    const taskElement = document.createElement("li"); // on initialise taskElement qui sera en li
    taskElement.innerHTML = `
      <span>${task.name}</span>
      <select>
        <option value="Travail">Travail</option>
        <option value="Ecole">Ecole</option>
        <option value="Personnel">Personnel</option>
      </select>
      <button class="edit-button">Editer</button>
      <button class="delete-button">Supprimer</button>
      <button class="complete-button">${
        task.completed ? "Terminé" : "Non Terminé"
      }</button>`;
    // Le HTML de la tache, qui contient les catégorie Travail, Ecole et Personnel et le bouton "complete-button" qui defini si une tache est finie ou non

    const categorySelect = taskElement.querySelector("select"); // cette ligne cree une variable categorySelect qui va recuperer l'element "select" ( au dessus le <select></select>)
    categorySelect.value = task.category; // met la valeur de task.categorie dans la nouvelle variable categorie.select et il va dans le .value
    categorySelect.addEventListener("change", () => {
      // c'est une variable qui ecoute et qui attend qu'il se passe quelque chose pour agir, ici c'est sur un changement
      task.category = categorySelect.value;
      saveTasks();
      // ca permet de ssauvegarder la catégorie mise à jour dans la tâche lorsqu'on change la sélection de la liste déroulante.
    });

    const editButton = taskElement.querySelector(".edit-button"); // on cree une variable editButton qui va recuperer l"element ".edit-button"
    editButton.addEventListener("click", () => {
      // si on clique sur le bouton editer ...
      editTask(index); // on edite le nom de la tâche
    });

    const deleteButton = taskElement.querySelector(".delete-button"); // on cree une variable deleteButton qui va recuperer l'element ".delete-button"
    deleteButton.addEventListener("click", () => {
      // si on clique sur le bouton delete ...
      deleteTask(index); // on supprime la tâche
    });

    const completeButton = taskElement.querySelector(".complete-button"); // on créé une variable completeButton qui va recuperer l'element ".complete-button"
    completeButton.addEventListener("click", () => {
      // si on clique sur le bouton termine ...
      completeTask(index); // on met a jourss la tâche ( terminée ou non terminée)
    });

    taskList.appendChild(taskElement); // Cette ligne ajoute chaque élément "taskElement" à la fin de la liste de tâches taskList
    //Doncc haque tâche qui est ajoutée à "taskList" sera affichée dans la page web
  });
}

// Fonction pour éditer une tâche de la liste
function editTask(index) {
  // -> creation de la fonction editTask(index)
  const task = tasks[index]; // creation d'une variable task qui prends en compte une tache en tableau grace à l'index fourni en parametre
  const newTaskName = prompt("Modifier la tâche :", task.name); // Creation d'_une variable newTaskName et affiche une boite de diaglogue pour la modification de la tâche selectionnée
  if (newTaskName) {
    // si c'est un nouveau nom..
    task.name = newTaskName; // on update le nom
    displayTasks(); // on l'affiche
    saveTasks(); // on sauvegarde en local dans un cookie
  }
}

// Fonction pour supprimer une tâche de la liste
function deleteTask(index) {
  // -> creation de la fonction deleteTask(index)
  tasks.splice(index, 1); // cette ligne supprime un element indiqué par l'index et est limite à 1 avec le ", 1"
  displayTasks(); // on l'affiche
  saveTasks(); // on sauvegarde en local dans un cookie
}

// Fonction pour marquer une tâche comme terminée ou non terminée
function completeTask(index) {
  // -> creation de la fonction completeTask(index)
  tasks[index].completed = !tasks[index].completed; // elle permet de mettre une tâche
  displayTasks(); // on l'affiche
  saveTasks(); // on sauvegarde en local dans un cookie
}

// Fonction pour sauvegarder les tâches dans le localStorage
function saveTasks() {
  // creation de la fonction saveTasks
  localStorage.setItem("tasks", JSON.stringify(tasks)); // utilisation de l'api webstorage de JS pour stocker en local
}

// Fonction pour charger les tâches depuis le localStorage
function loadTasks() {
  // creation de la fonction loadTasks
  const storedTasks = JSON.parse(localStorage.getItem("tasks"));
  if (storedTasks) {
    // s'il y a des valeurs dans storedTasks...
    tasks = storedTasks; // on met les parametres de tasks dans storedTasks
    displayAllTasks();
  } // on affiche toutes les taches
}

// Cette fonctionnalité permet d'exporter les taches dans un fichier .txt
function exportTasks() {
  let exportText = "";
  for (let i = 0; i < tasks.length; i++) {
    exportText +=
      (tasks[i].completed ? "Terminée" : "En cours") +
      " -- " +
      tasks[i].name +
      " (catégorie: " +
      tasks[i].category +
      ")" +
      "\n";
  } // Si la tache est terminée ou non + le nom de la tache +  la catégorie puis retours a la ligne avec le \n
  const downloadLink = document.createElement("a");
  downloadLink.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(exportText)
  );
  downloadLink.setAttribute("download", "taches.txt"); // on donne le nom du fichier lors de l'exportation
  downloadLink.style.display = "none";
  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);
}

// Cette fonctionnalité permet de rechercher une tache avec une ou plusieurs lettre dans le(s) mot(s)
function searchTasks() {
  const searchText = searchTaskInput.value.trim().toLowerCase();
  const filteredTasks = tasks.filter((task) =>
    task.name.toLowerCase().includes(searchText)
  );
  displayTasks(filteredTasks);
}

// Événement pour ajouter une nouvelle tache
addTaskButton.addEventListener("click", addTask); // sur le clique du bouton pour ajouter une tache
newTaskInput.addEventListener("keydown", (event) => {
  if (event.key === "Enter") {
    // pareil mais au lieu que ce soit un clique c'est d'appuyer sur ENTRER
    addTask();
  } // on ajoute la tache
});

// ---------- Tous les ecouteurs d'évenements ----------

// Afficher toute les taches avec l'ecouteur d'évenements
allTasksButton.addEventListener("click", displayAllTasks);

// Afficher les tâches filtrées ( terminées et non terminées ) avec l'ecouteur d'evenements
completedTasksButton.addEventListener("click", displayCompletedTasks);
uncompletedTasksButton.addEventListener("click", displayUncompletedTasks);

// Sur le clique, afficher les taches qui comporte les lettres entrées
searchButton.addEventListener("click", searchTasks);

//
const exportTasksButton = document.getElementById("export-tasks");
exportTasksButton.addEventListener("click", exportTasks);

searchTaskInput.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    searchTasks();
  }
});

// Chargement initial des tâches
loadTasks();
